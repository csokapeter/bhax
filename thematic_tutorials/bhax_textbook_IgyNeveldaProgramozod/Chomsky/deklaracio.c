#include <stdio.h>
int* f();//egészre mutató mutatót visszaadó függvény

int main(void)
{
	int x; //egész
	int* y; //egészre mutató mutató
	int* z = &x; //egész referenciája
	int a[10]; //egészek tömbje
	int* uh = &a[5]; //egészek tömbjének referenciája (nem az első elemé)
	int* pp[5]; //egészre mutató mutatók tömbje
	int* (*f_ptr)(); //egészre mutató mutatót visszaadó függvényre mutató mutató
	int (*f2 (int z3))(int z1, int z2); //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
	int (*(*f3) (int u3))(int u1, int u2); //függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
	return 0;
}