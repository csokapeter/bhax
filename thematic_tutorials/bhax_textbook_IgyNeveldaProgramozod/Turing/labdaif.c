//futtatas: gcc labdaif -o labdaif -lncurses
#include <ncurses.h>
#include <unistd.h>

int main()
{
	initscr();
	noecho();

	int yBeg, xBeg, yMax, xMax;
	getbegyx(stdscr, yBeg, xBeg);
	getmaxyx(stdscr, yMax, xMax);
	yMax -= 1;
	xMax -= 1;

	WINDOW * win = newwin(yMax, xMax, yBeg, xBeg);
	refresh();

	int y = 0;
	int x = 0;
	int ySpeed = 1;
	int xSpeed = 1;

	while(true)
	{
		clear();
		mvprintw(y,x,"o");
		x += xSpeed;
		y += ySpeed;
		refresh();
		usleep(100000);
		if(x == xBeg || x == xMax)
			xSpeed *= -1;
		if(y == yBeg || y == yMax)
			ySpeed *= -1;
	}
	endwin();
	return 0;
}