//futtatas: gcc labda -o labda -lncurses
#include <ncurses.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	initscr();
	noecho();

	int yMin, xMin, yMax, xMax;
	getbegyx(stdscr, yMin, xMin);
	getmaxyx(stdscr, yMax, xMax);
	yMax -= 1;
	xMax -= 1;

	WINDOW * win = newwin(yMax, xMax, yMin, xMin);
	refresh();

	int xj = 0, xk = 0, yj = 0, yk = 0;

	while(true)
	{
		xj = (xj - 1) % (xMax * 2);
		xk = (xk + 1) % (xMax * 2);

		yj = (yj - 1) % (yMax * 2);
		yk = (yk + 1) % (yMax * 2);

		clear ();
		mvprintw (abs ((yj + (yMax * 2 - yk)) / 2), abs ((xj + (xMax * 2 - xk)) / 2), "o");

		refresh ();
		usleep (100000);

	}
	endwin();
	return 0;
}