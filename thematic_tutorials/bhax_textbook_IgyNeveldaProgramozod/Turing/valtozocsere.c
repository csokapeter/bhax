#include <stdio.h>

int main()
{
    int a = 11;
    int b = 6;

    printf("Csere elott a: %d, d:%d\n",a,b);

    a = a-b;
    b = b+a;
    a = b-a;

    printf("Csere utan a: %d, d:%d\n\n",a,b);
}