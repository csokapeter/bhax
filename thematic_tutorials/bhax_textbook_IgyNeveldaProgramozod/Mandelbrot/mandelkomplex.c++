#include <iostream>
#include <png++/png.hpp>
#include <sys/times.h>
#include <complex>
#include <cmath>

#define MERET 600
#define ITER_HAT 32000

void
mandel (int kepadat[MERET][MERET]) {

    clock_t delta = clock ();
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

    double a = -2.0, b = .7, c = -1.35, d = 1.35;
    int szelesseg = MERET;
    int magassag = MERET;
    int iteraciosHatar = ITER_HAT;

    double dx = (b - a) / szelesseg;
    double dy = (d - c) / magassag;
    double reC, imC, reZ, imZ;
    int iteracio = 0;
    for (int j = 0; j < magassag; ++j)
    {
        for (int k = 0; k < szelesseg; ++k)
        {
            reC = a + k * dx;
            imC = d - j * dy;

            std::complex<double> c ( reC, imC );
            std::complex<double> z_n ( 0.0, 0.0 );
            iteracio = 0;


            while (std::abs(z_n)< 4 && iteracio < iteraciosHatar)
            {
                z_n = z_n*z_n +c;
                ++iteracio;

            }

            kepadat[j][k] = iteracio;
        }
    }

    times (&tmsbuf2);
    std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

    delta = clock () - delta;
    std::cout << std::endl << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;
}

int
main (int argc, char *argv[])
{
    using namespace std::complex_literals;

    if (argc != 2)
    {
        std::cout << "Hasznalat: ./mandelpng fajlnev";
        return -1;
    }

    int kepadat[MERET][MERET];

    mandel(kepadat);

    png::image < png::rgb_pixel > kep (MERET, MERET);

    for (int j = 0; j < MERET; ++j)
    {
        for (int k = 0; k < MERET; ++k)
        {
            kep.set_pixel (k, j,
                           png::rgb_pixel (255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT));
        }
    }

    kep.write (argv[1]);
    std::cout << argv[1] << " mentve" << std::endl;

}
